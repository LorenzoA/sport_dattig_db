<?php
include_once('./configbdd.php');
require_once('./requestsql.php');

setcookie("mail", '', time() + 60 * 60 * 24 * 30);
setcookie("firstname", '', time() + 60 * 60 * 24 * 30);
setcookie("lastname", '', time() + 60 * 60 * 24 * 30);
setcookie("depart", '', time() + 60 * 60 * 24 * 30);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/journal/bootstrap.min.css" integrity="sha384-QDSPDoVOoSWz2ypaRUidLmLYl4RyoBWI44iA5agn6jHegBxZkNqgm2eHb6yZ5bYs" crossorigin="anonymous">
    <style>
        .container-fluid {
            background-image: url('./img/Comment-faire-du-sport-en-couple.jpg');
            min-height: 100vh;
            background-size: cover;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .maintitle {
            margin-top: 10vh;
            color: red;
        }

        .connect {
            border-radius: 5px;
            padding: 10px 5px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-around;
            margin-top: 30vh;
            background-color: rgba(255, 255, 255, 0.5);
        }
    </style>
</head>

<body>
    <?php include("navbar.html") ?>
    <div class="container-fluid">
        <div class="row maintitle">
            <h1>Welcome on SportBuds</h1>
        </div>
        <div class="row">
            <div class="connect">
                <?php
                // VERIFICATION DE L' EXISTENCE DE L'EMAIL DANS LA BDD
                if (isset($_POST['email'])) {
                    $bdd = getPDO();
                    $email = htmlspecialchars($_POST['email']);
                    $emailSanitize = filter_var($email, FILTER_VALIDATE_EMAIL);
                    $verification = $bdd->prepare(EMAIL_VERIFICATION);
                    $verification->execute(array($email));
                    $data = $verification->fetch();
                    $ligneMail = $verification->rowCount();
                    var_dump($data);
                    if (!empty($data)) {
                        $mail = $data->mail;
                        $firstName = $data->prenom;
                        $lastName = $data->nom;
                        $depart = $data->departement;

                        //déclaration des cookies

                        setcookie("mail", $mail, time() + 60 * 60 * 24 * 30);
                        setcookie("firstname", $firstName , time() + 60 * 60 * 24 * 30);
                        setcookie("lastname", $lastName, time() + 60 * 60 * 24 * 30);
                        setcookie("depart", $depart, time() + 60 * 60 * 24 * 30);

                        header("Location: recherche.php?firstname=$firstName&lastname=$lastName&email=$email&depart=$depart");
                    } else header('Location: ajout.php');
                }
                ?>
                <p>Veuillez entrer votre email pour vous connecter:</p>
                <form action="index.php" method="post">
                    <p>
                        <input type="email" name="email" />
                        <input type="submit" value="Valider" />
                    </p>
                </form>
                <p>Si votre email est inconnu vous serez redirigé vers l'inscription</p>
                <a href="ajout.php">ajout</a>
            </div>
        </div>
    </div>



</body>

</html>