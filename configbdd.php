<?php
const DB_DRIVER = 'mysql';
const DB_HOST = 'localhost';
const DB_NAME = 'sport_dating_db';
const DEFAULT_DB_LOGIN = 'root';
const DEFAULT_DB_PWD = '';


// $bdd = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DEFAULT_DB_LOGIN, DEFAULT_DB_PWD);
// $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
function getPDO()
{
    try {
        $bdd = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DEFAULT_DB_LOGIN, DEFAULT_DB_PWD, array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ));
    } catch (Exception $e) {
        
        die('Erreur:' . $e->getMessage());
    }

    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $bdd;
}

?>