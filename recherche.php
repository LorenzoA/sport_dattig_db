<?php
//
//création verificatiopn des cookies
// if (isset($_POST['prenom']) && !empty($_POST['prenom']) && !empty($_POST['nom']) && isset($_POST['nom'])) {

//     setcookie('lastname', $_POST['nom'], time() + 3600 * 24, '/', '', false, false);
//     setcookie('firstname', $_POST['prenom'], time() + 3600 * 24, '/', '', false, false);
//     setcookie('email', $_GET["email"], time() + 24 * 3600, null, null, false, true);
//     setcookie('depart', $_GET["depart"], time() + 24 * 3600, null, null, false, true);
// } else 
 if (isset($_GET['firstname']) && isset($_GET['lastname']) && !empty($_GET['firstname']) && !empty($_GET['lastname'])) {
    setcookie('email', $_GET["email"], time() + 24 * 3600, null, null, false, true);
    setcookie('depart', $_GET["depart"], time() + 24 * 3600, null, null, false, true);
    setcookie('firstname', $_GET["firstname"], time() + 24 * 3600, null, null, false, true);
    setcookie('lastname', $_GET["lastname"], time() + 24 * 3600, null, null, false, true);
 }else if(empty($_GET['email'])){
    
 } 


include_once('./configbdd.php');
include_once('./requestsql.php');
require_once('./function.php');

//récupération des infos de l'users sur les différentes tables.
if(!empty($_GET['email'])){
    $mail = $_GET['email'];
    setcookie('email', $mail, time() + 24 * 3600, null, null, false, true);
}else if(empty($_GET['email'])){
    $mail = $_COOKIE['email'];
}



$resAllPersData = reqAllUserData($mail);

$userSportId = $resAllPersData['id_sport_id'];
$userNiveau = $resAllPersData['niveau'];
$userSportName = $resAllPersData['nom_sport'];

$userDepartement = $resAllPersData['departement'];
$firstname = $resAllPersData['prenom'];
$lastname = $resAllPersData['nom'];


//recuperation des infos pour la recherche de profils par critères
if (isset($_POST['sportProfile']) && isset($_POST['departementProfile'])) {
    if (!empty($_POST['sportProfile'])&&!empty($_POST['departementProfile'])) {
        $sportNameProfile = $_POST['sportProfile'];
        $departementProfile = $_POST['departementProfile'];
    }else echo "";
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/journal/bootstrap.min.css" integrity="sha384-QDSPDoVOoSWz2ypaRUidLmLYl4RyoBWI44iA5agn6jHegBxZkNqgm2eHb6yZ5bYs" crossorigin="anonymous">
    <style>
        body{background-color:#ece4db}
         .profilesBySearch{display:flex; flex-direction:row; flex-wrap: wrap; justify-content: space-evenly;}
         .profileBySameSport{display:flex; flex-direction:row; flex-wrap: wrap; justify-content:space-evenly;}
         .welcomeMessage{margin:5vh auto;text-align:center;}
         .formSearch{border:1px solid black;padding:15px;background-color:aliceblue;border-radius:8px;width:max-content;}
        .searchPage{background: #BA5370;  /* fallback for old browsers */
background: -webkit-linear-gradient(to top, #F4E2D8, #BA5370);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to top, #F4E2D8, #BA5370); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
}
    </style>
    <title>Recherche</title>
</head>

<body class=" searchPage">
    <?php include('./navbar.html'); ?>
    <div class="container-fluid">
        <h2 class="welcomeMessage">Hey <?php printf($lastname . " "); printf($firstname);?> !
            Prêt à trouver l'amour ? </h2>
        
        <!-- FORMULAIRE RECHERCHE PROFILS PAR SPORT ET DEPARTEMENT -->
        <legend>Find your mate</legend>
        <div class="formSearch d-flex flex-row mb-3">
            <form action="recherche.php" method="post">
                <div class="form-group d-flex flex-column">
                <label for="exampleSelect1">Sport souhaité</label>
                    <select name="sportProfile" placeholder="Choisissez votre sport" id="">
                        <?php
                        getFullSportList();
                        ?>
                    </select>
                </div>
                <div class="form-group d-flex flex-column">
                <label for="exampleSelect1">Dans quel département ? </label>
                    <select name="departementProfile" placeholder="departement" id="">
                        <?php
                        getFullDepartementList();
                        ?>
                    </select>
                    <input class="mt-1" type="submit" value="voir les profils">
                </div>
            </form>
        </div>
      <?php  
      if(!empty($_POST['sportProfile']) && !empty($_POST['departementProfile'])){
             //RESULTATS DE RECHERCHE DE PROFILS PAR CRITERES
        printf(" <h2>Liste des incrits pratiquant le " . $sportNameProfile
       ." dans le "   . $departementProfile .": </h2>"); ?>
        <div class="profilesBySearch">
            <?php
            displaySportBySearch($sportNameProfile, $departementProfile);
            ?>
        </div>
        <?php }
        ?>
        <?php printf(" <h2>Liste des incrits pratiquant le " . $userSportName
       .": </h2>"); ?>
        <div class="profileBySameSport">
        <?php
            displaySameSportUsers($userSportName);
            ?>
        </div>
       
        <a href="ajout.php">ajout</a>
    </div>
</body>

</html>