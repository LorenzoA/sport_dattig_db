<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/journal/bootstrap.min.css" integrity="sha384-QDSPDoVOoSWz2ypaRUidLmLYl4RyoBWI44iA5agn6jHegBxZkNqgm2eHb6yZ5bYs" crossorigin="anonymous">
    <style>.backgroundSign{background-image:url('./img/2082636.jpg');min-height:100vh;background-size: cover;}
            .addForm{background-color:rgba(255,255,255,0.5);border-radius: 8px;margin:10vh auto;padding-top:10px;padding-bottom:10px;}
</style>
    <title>Inscription</title>
</head>

<body>
<div class="container-fluid backgroundSign">
    <?php include("navbar.html");
    // require_once("style\style.css"); 
    
    require_once('./configbdd.php');
    require_once('./requestsql.php');
    $bdd = getPDO();
    require_once "function.php";
    if (!empty($_COOKIE['email']) && !empty($_COOKIE['nom']) && !empty($_COOKIE['prenom']) && !empty($_COOKIE['departement'])) {
        $nom = $_COOKIE['nom'];
        $prenom = $_COOKIE['prenom'];
        $mail = $_COOKIE['mail'];
        $departement = $_COOKIE['depart'];
    }
    
    

    if (isset($_POST['lastName']) && isset($_POST['email']) && isset($_POST['firstName']) && isset($_POST['departement']) && isset($_POST['niveau'])) {

        $firstName = htmlspecialchars($_POST['firstName']);
        $email = htmlspecialchars($_POST['email']);
        $lastName = htmlspecialchars($_POST['lastName']);
        $sport = htmlspecialchars($_POST['sport']);
        $level = $_POST['niveau'];


        $departement = htmlspecialchars($_POST['departement']);
        $data = emailVerification($email);
        if (empty($data)) {
            if (strlen($firstName) <= 100) {
                if (strlen($email) <= 320) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                        addUserInfos($firstName, $lastName, $departement, $email);

                        //RECUPERATION DE L'ID DE PERSONNE

                        $personalID = getUserId($email);

                        //RECUPERATION DE L'ID DU SPORT

                        $idsport = getUserSportId($sport);

                        //insertion table pratique de

                        insertUserPratique($personalID, $idsport, $level);
                    } else {
                        echo ('Email non conforme');
                    }
                } else {
                    echo ('Email trop long');
                }
            } else {
                echo ('Prénom trop long');
            }
        } else {
            echo ("L'adresse  " . $email . "est déjà utilisée, nous vous invitons à vous connecter");
        }
        header("Location: recherche.php?firstname=$firstName&lastname=$lastName&id=$personalID&sportid=$idsport&depart=$departement&email=$email");
    } else echo ('kuzzgdfmaoufbpaefbi');
    
    ?>
    
        <div class="container addForm">
            <form action="ajout.php" method="post">
                <fieldset>
                    <legend>Inscription</legend>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Prénom</label>
                        <input type="text" name="firstName" class="form-control" aria-describedby="emailHelp" placeholder="Prénom">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nom</label>
                        <input type="text" name="lastName" class="form-control" placeholder="Nom">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email </label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email@example.com">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Département</label>
                        <input type="text" name="departement" class="form-control" placeholder="Exemple : 75">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Votre sport</label>
                        <select class="form-control" name="sport" id="exampleSelect1">
                            <?php
                            //FONCTION POUR AFFICHER LA LISTE DES SPORTS
                            getFullSportList();
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect2">Votre niveau</label>
                        <select name="niveau" class="form-control" id="exampleSelect2">
                            <option value="debutant">Débutant</option>
                            <option value="confirmé">Confirmé</option>
                            <option value="veteran">Vétéran</option>
                            <option value="professionel">Professionel</option>
                        </select>
                    </div>
                    <button type="submit" value="Inscription" class="btn btn-primary">Inscription</button>
                    <!-- <input type="submit" value="Inscription"> -->
        
            </form>
        </div>
    </div>
    <!-- <form action="ajout.php" method="post">
        <input type="text" placeholder="name" name="firstName" id="">
        <input type="text" placeholder="last name" name="lastName" id="">
        <input type="email" placeholder="email" name="email" id="">
        <input type="text" name="departement" id="">

        <select name="sport">
            <?php
            //FONCTION POUR AFFICHER LA LISTE DES SPORTS
            getFullSportList();
            ?>
        </select>
        <select name="niveau" placeholder="Choisissez votre niveau" id="">
        <option value="debutant">Débutant</option>
                    <option value="confirmé">Confirmé</option>
                    <option value="veteran">Vétéran</option>
                    <option value="professionel">Professionel</option>
        </select>

        <input type="submit" value="Inscription">
    </form> -->


    

</body>

</html>