<?php

require_once('./requestsql.php');
require_once("configbdd.php");
//TRAITEMENT PAGE INDEX

function emailVerification($email)
{
    $bdd = getPDO();

    $verification = $bdd->prepare(EMAIL_VERIFICATION);
    $verification->execute(array($email));
    $data = $verification->fetch();

    return $data;
};


function addUserInfos($firstName, $lastName, $departement, $email)
{

    $bdd = getPDO();

    $addPerson = $bdd->prepare(ADD_PERSON);
    $addPerson->execute(array(

        'nom' => $firstName,
        'prenom' => $lastName,
        'departement' => $departement,
        'email' => $email
    ));
    return $addPerson;
}

function getUserId($email)
{
    $bdd = getPDO();
    $getID = $bdd->prepare(GET_PERSONAL_ID);
    $getID->execute(array(':mail' => $email));
    $dataID = $getID->fetch(PDO::FETCH_ASSOC);
    $personalID = intval($dataID["id"]);
    return $personalID;
}

function getUserSportId($sport)
{
    $bdd = getPDO();
    $getSportID = $bdd->prepare(GET_SPORT_ID);
    $getSportID->execute(array(':sport' => $sport));
    $dataSportID = $getSportID->fetch(PDO::FETCH_ASSOC);
    $idsport = $dataSportID['id'];
    return $idsport;
}

function insertUserPratique($personalID, $idsport, $level)
{
    $bdd = getPDO();
    $insertPratique = $bdd->prepare(INSERT_PRATIQUE);
    $insertPratique->execute(array(
        ':idPerson' => $personalID,
        ':id_sport' => $idsport,
        ':niveau' => $level
    ));
}
function getFullSportList()
{
    $bdd = getPDO();
    $sportList = $bdd->query(GET_SPORT_LIST);
    while ($dataSportList = $sportList->fetch(PDO::FETCH_ASSOC)) {

        printf("<option value=" . htmlspecialchars($dataSportList['nom_sport']) . " >" .  $dataSportList['nom_sport'] . "</option>");
    }
}



//TRAITEMENT PAGE RECHERCHE
function reqAllUserData($mail)
{
    $bdd = getPDO();
    $findAllPersData = $bdd->prepare('SELECT * FROM sd_personne,sd_pratique,sd_sport WHERE sd_personne.id = sd_pratique.id_personne_id AND sd_pratique.id_sport_id = sd_sport.id AND sd_personne.mail = :mail');
    $findAllPersData->execute(array(':mail' => $mail));
    $resAllPersData = $findAllPersData->fetch(PDO::FETCH_ASSOC);
    return $resAllPersData;
}

function getFullDepartementList()
{
    $bdd = getPDO();
    $departmentList = $bdd->query("SELECT DISTINCT departement FROM sd_personne ");
    while ($dataDepartementList = $departmentList->fetch(PDO::FETCH_ASSOC)) {
        printf("<option value=" . htmlspecialchars($dataDepartementList['departement']) . " >" .  $dataDepartementList['departement'] . "</option>");
    }
}

function displaySameSportUsers($userSportName)
{
    $bdd = getPDO();
    $displaySport = $bdd->prepare("SELECT P.nom, P.prenom, P.departement, U.niveau, S.nom_sport FROM sd_pratique AS U 
    INNER JOIN sd_personne AS P ON P.id = U.id_personne_id 
    INNER JOIN sd_sport AS S ON S.id = U.id_sport_id 
    WHERE S.nom_sport = :sport ");
    $displaySport->execute(array(':sport' => $userSportName));
    while ($dataTodisplay = $displaySport->fetch()) {
        if ($dataTodisplay == 0) {
            printf("<p> Désolé personne ne pratique le même sport que vous </p>");
        } else
            printf("
        <div class='card mb-3' style='width: 15rem;'>
            <img src='img/2nix140800145.jpg' class='card-img-top' alt='...'>
            <div class='card-body'>
                <h5 class='card-title'>" . $dataTodisplay['nom']  . " " . $dataTodisplay['prenom']  . " </h5>
                <p class='card-text'> Département :" . $dataTodisplay['departement'] . "</p>
            </div>
            <ul class='list-group list-group-flush'>
                <li class='list-group-item'>" . $dataTodisplay['nom_sport'] . "</li>
                <li class='list-group-item'>" . $dataTodisplay['niveau'] . "</li>
            </ul>
            </div>
");
    }
}

function displaySportBySearch($sportNameProfile, $departementProfile)
{
    $bdd = getPDO();
    $displayProfilBySearch = $bdd->prepare(DISPLAY_SPORT_BY_NAME);
    $displayProfilBySearch->execute(array(':sport' => $sportNameProfile, ':depart' => $departementProfile));
    while ($dataProfilesBySearch = $displayProfilBySearch->fetch(PDO::FETCH_ASSOC)) {

        printf("
        <div class='card mb-3' style='width: 15rem;'>
        <img src='img/2nix140800145.jpg' class='card-img-top' alt='...'>
        <div class='card-body'>
          <h5 class='card-title'>" . $dataProfilesBySearch['nom']  . " " . $dataProfilesBySearch['prenom']  . " </h5>
          <p class='card-text'> Département :" . $dataProfilesBySearch['departement'] . "</p>
        </div>
        <ul class='list-group list-group-flush'>
          <li class='list-group-item'>" . $dataProfilesBySearch['nom_sport'] . "</li>
          <li class='list-group-item'>" . $dataProfilesBySearch['niveau'] . "</li>
        </ul>
      </div>");
    }
    if ($dataProfilesBySearch == 0) {
        printf("Désolé c'est la fin de la liste des pratiquants de " . $sportNameProfile . " " . " dans le " .   $departementProfile . "");
    } else echo ("");
}
